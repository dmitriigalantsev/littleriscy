# Docker Images
## Dependencies
After installing Docker for your OS start Docker.

```
sudo dockerd
```

## Local Usage
### Build
Build the Docker image for littleriscy_x86-64 from the root directory.

```
sudo docker build -t littleriscy_x86-64 -f ./docker/littleriscy_x86-64/Dockerfile .
```

### Use
Start a Bash shell inside of the Docker image.

```
sudo docker run -it littleriscy_x86-64 bash
```

## Build and Push an Image to GitLab
Login to the Gitlab registry first.

```
sudo docker login registry.gitlab.com
```

Build and push an image by running the following in the folder with the Docker file. The example is for building the littleriscy_x86-64 image from the root directory.

```
cd docker/littleriscy_x86-64/
sudo docker build -t registry.gitlab.com/lao.alex.512/littleriscy:littleriscy_x86-64 .
sudo docker push registry.gitlab.com/lao.alex.512/littleriscy:littleriscy_x86-64
```
