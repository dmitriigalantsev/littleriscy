/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file test/software/liblittleriscy_emulator/test_cpu_instruction_decode.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2021
 *
 * @brief Test the CPU instruction decoder
 */

#include <cstdint>
#include <random>

#include "gtest/gtest.h"

#include "cpu_instruction_decode.h"

/**
 * @brief Test decode_load
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = width[2:0]
 * instruction[19:15] = base[4:0]
 * instruction[31:20] = offset[11:0]
 */
TEST(test_cpu_instruction_decode, decode_load) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_dest_base(0, 31);
    std::uniform_int_distribution<unsigned int> uniform_width(0, 7);
    std::uniform_int_distribution<int> uniform_offset(-2048, 2047);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b0000011;

    uint8_t dest = 0;
    uint8_t base = 0;
    uint8_t width = 0;
    int16_t offset = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        dest = uniform_dest_base(rand_engine);
        base = uniform_dest_base(rand_engine);
        width = uniform_width(rand_engine);
        offset = uniform_offset(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((dest & 0x1F) << 7);
        instruction = instruction | ((width & 0x07) << 12);
        instruction = instruction | ((base & 0x1F) << 15);
        instruction = instruction | ((offset & 0xFFF) << 20);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, dest);
        EXPECT_EQ(test_cpu_state.funct3, width);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, base);
        EXPECT_EQ(test_cpu_state.signed_immediate, offset);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_READ);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_SIGNED_IMMEDIATE);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_op_imm
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = src[4:0]
 * instruction[31:20] = I-immediate[11:0] or U-immediate[11:0]
 */
TEST(test_cpu_instruction_decode, decode_op_imm) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_dest_src(0, 31);
    std::uniform_int_distribution<unsigned int> uniform_funct3(0, 7);
    std::uniform_int_distribution<int> uniform_imm(-2048, 2047);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b0010011;

    uint8_t dest = 0;
    uint8_t funct3 = 0;
    uint8_t src = 0;
    int16_t imm = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        dest = uniform_dest_src(rand_engine);
        funct3 = uniform_funct3(rand_engine);
        src = uniform_dest_src(rand_engine);
        imm = uniform_imm(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((dest & 0x1F) << 7);
        instruction = instruction | ((funct3 & 0x07) << 12);
        instruction = instruction | ((src & 0x1F) << 15);
        instruction = instruction | ((imm & 0xFFF) << 20);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, dest);
        EXPECT_EQ(test_cpu_state.funct3, funct3);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, src);

        // Always sign extended
        EXPECT_EQ(test_cpu_state.signed_immediate, imm);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, imm);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_REGISTER);

        if (funct3 == test_cpu_state.FUNCT3_OP_IMM_SLTIU) {
            EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_UNSIGNED_IMMEDIATE);
        } else {
            EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_SIGNED_IMMEDIATE);
        }

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_auipc
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[31:12] = U-immediate[31:12]
 */
TEST(test_cpu_instruction_decode, decode_auipc) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_dest(0, 31);
    std::uniform_int_distribution<int> uniform_imm(-1048576, 1048575);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b0010111;

    uint8_t dest = 0;
    int32_t imm = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        dest = uniform_dest(rand_engine);
        imm = uniform_imm(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((dest & 0x1F) << 7);
        instruction = instruction | ((imm & 0xFFFFF) << 12);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, dest);

        // Always sign extended
        EXPECT_EQ(test_cpu_state.signed_immediate, imm << 12);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.funct3, 0);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_SIGNED_IMMEDIATE);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_PROGRAM_COUNTER);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_store
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = offset[4:0]
 * instruction[14:12] = width[2:0]
 * instruction[19:15] = base[4:0]
 * instruction[24:20] = src[4:0]
 * instruction[31:25] = offset[11:5]
 */
TEST(test_cpu_instruction_decode, decode_store) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_base_src(0, 31);
    std::uniform_int_distribution<unsigned int> uniform_width(0, 7);
    std::uniform_int_distribution<int> uniform_offset(-2048, 2047);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b0100011;

    uint8_t base = 0;
    uint8_t src = 0;
    uint8_t width = 0;
    int16_t offset = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        base = uniform_base_src(rand_engine);
        src = uniform_base_src(rand_engine);
        width = uniform_width(rand_engine);
        offset = uniform_offset(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((offset & 0x1F) << 7);
        instruction = instruction | ((width & 0x07) << 12);
        instruction = instruction | ((base & 0x1F) << 15);
        instruction = instruction | ((src & 0x1F) << 20);
        instruction = instruction | (((offset >> 5) & 0x1FF) << 25);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.funct3, width);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, base);
        EXPECT_EQ(test_cpu_state.right_operand_register_num, src);
        EXPECT_EQ(test_cpu_state.signed_immediate, offset);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_WRITE);
        EXPECT_EQ(test_cpu_state.dest_register_num, 0);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_NONE);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_op
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = src1[4:0]
 * instruction[24:20] = src2[4:0]
 * instruction[31:25] = funct7[6:0]
 */
TEST(test_cpu_instruction_decode, decode_op) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_dest_src1_src2(0, 31);
    std::uniform_int_distribution<unsigned int> uniform_funct3(0, 7);
    std::uniform_int_distribution<unsigned int> uniform_funct7(0, 127);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b0110011;

    uint8_t dest = 0;
    uint8_t src1 = 0;
    uint8_t src2 = 0;
    uint8_t funct3 = 0;
    uint8_t funct7 = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        dest = uniform_dest_src1_src2(rand_engine);
        src1 = uniform_dest_src1_src2(rand_engine);
        src2 = uniform_dest_src1_src2(rand_engine);
        funct3 = uniform_funct3(rand_engine);
        funct7 = uniform_funct7(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((dest & 0x1F) << 7);
        instruction = instruction | ((funct3 & 0x07) << 12);
        instruction = instruction | ((src1 & 0x1F) << 15);
        instruction = instruction | ((src2 & 0x1F) << 20);
        instruction = instruction | ((funct7 & 0x7F) << 25);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, dest);
        EXPECT_EQ(test_cpu_state.funct3, funct3);
        EXPECT_EQ(test_cpu_state.funct7, funct7);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, src1);
        EXPECT_EQ(test_cpu_state.right_operand_register_num, src2);


        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);
        EXPECT_EQ(test_cpu_state.signed_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_lui
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[31:12] = U-immediate[31:12]
 */
TEST(test_cpu_instruction_decode, decode_lui) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_dest(0, 31);
    std::uniform_int_distribution<int> uniform_imm(-1048576, 1048575);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b0110111;

    uint8_t dest = 0;
    int32_t imm = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        dest = uniform_dest(rand_engine);
        imm = uniform_imm(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((dest & 0x1F) << 7);
        instruction = instruction | ((imm & 0xFFFFF) << 12);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, dest);

        EXPECT_EQ(test_cpu_state.unsigned_immediate, imm << 12);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.funct3, 0);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_UNSIGNED_IMMEDIATE);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_NONE);
        EXPECT_EQ(test_cpu_state.signed_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_branch
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[7]     = offset[11]
 * instruction[11:8]  = offset[4:1]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = src1[4:0]
 * instruction[24:20] = src2[4:0]
 * instruction[30:25] = offset[10:5]
 * instruction[31]    = offset[12]
 */
TEST(test_cpu_instruction_decode, decode_branch) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_src1_src2(0, 31);
    std::uniform_int_distribution<unsigned int> uniform_funct3(0, 7);
    std::uniform_int_distribution<int> uniform_offset(-4096, 4095);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b1100011;

    uint8_t src1 = 0;
    uint8_t src2 = 0;
    uint8_t funct3 = 0;
    int32_t offset = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        src1 = uniform_src1_src2(rand_engine);
        src2 = uniform_src1_src2(rand_engine);
        funct3 = uniform_funct3(rand_engine);
        offset = uniform_offset(rand_engine) & 0xFFFFFFFE;  // LSB always 0

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((funct3 & 0x07) << 12);
        instruction = instruction | ((src1 & 0x1F) << 15);
        instruction = instruction | ((src2 & 0x1F) << 20);

        instruction = instruction | (((offset >> 11) & 0x1) << 7);
        instruction = instruction | (((offset >> 1) & 0xF) << 8);
        instruction = instruction | (((offset >> 5) & 0x3F) << 25);
        instruction = instruction | (((offset >> 12) & 0x1) << 31);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.funct3, funct3);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, src1);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.right_operand_register_num, src2);

        EXPECT_EQ(test_cpu_state.signed_immediate, offset);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);
        EXPECT_EQ(test_cpu_state.dest_register_num, 0);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_NONE);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_jalr
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = base[4:0]
 * instruction[31:20] = offset[11:0]
 */
TEST(test_cpu_instruction_decode, decode_jalr) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_dest_base(0, 31);
    std::uniform_int_distribution<unsigned int> uniform_funct3(0, 7);
    std::uniform_int_distribution<int> uniform_offset(-2048, 2047);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b1100111;

    uint8_t dest = 0;
    uint8_t funct3 = 0;
    uint8_t base = 0;
    int16_t offset = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        dest = uniform_dest_base(rand_engine);
        funct3 = uniform_funct3(rand_engine);
        base = uniform_dest_base(rand_engine);
        offset = uniform_offset(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((dest & 0x1F) << 7);
        instruction = instruction | ((funct3 & 0x07) << 12);
        instruction = instruction | ((base & 0x1F) << 15);
        instruction = instruction | ((offset & 0xFFF) << 20);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, dest);
        EXPECT_EQ(test_cpu_state.funct3, funct3);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, base);

        // Always sign extended
        EXPECT_EQ(test_cpu_state.signed_immediate, offset);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_SIGNED_IMMEDIATE);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_jal
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = dest[4:0]
 * instruction[19:12] = offset[19:12]
 * instruction[20]    = offset[11]
 * instruction[30:21] = offset[10:1]
 * instruction[31]    = offset[20]
 */
TEST(test_cpu_instruction_decode, decode_jal) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_dest(0, 31);
    std::uniform_int_distribution<int> uniform_offset(-1048576, 1048576);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b1101111;

    uint8_t dest = 0;
    int32_t offset = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        dest = uniform_dest(rand_engine);
        offset = uniform_offset(rand_engine) & 0xFFFFFFFE;  // LSB always 0

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((dest & 0x1F) << 7);

        instruction = instruction | (((offset >> 12) & 0xFF) << 12);
        instruction = instruction | (((offset >> 11) & 0x1) << 20);
        instruction = instruction | (((offset >> 1) & 0x3FF) << 21);
        instruction = instruction | (((offset >> 20) & 0x1) << 31);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, dest);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.signed_immediate, offset);
        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);

        EXPECT_EQ(test_cpu_state.funct3, 0);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, 0);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_NONE);
        EXPECT_EQ(test_cpu_state.right_operand_register_num, 0);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_NONE);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.funct12, 0);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}

/**
 * @brief Test decode_system
 *
 * instruction[6:0]   = opcode[6:0]
 * instruction[11:7]  = rd[4:0]
 * instruction[14:12] = funct3[2:0]
 * instruction[19:15] = rs1[4:0]
 * instruction[31:20] = funct12[11:0]
 */
TEST(test_cpu_instruction_decode, decode_system) {
    std::random_device r;
    std::default_random_engine rand_engine(r());
    std::uniform_int_distribution<unsigned int> uniform_rd_rs1(0, 31);
    std::uniform_int_distribution<unsigned int> uniform_funct3(0, 7);
    std::uniform_int_distribution<unsigned int> uniform_funct12(0, 4095);

    cpu_instruction_decode test_decoder;
    cpu_state test_cpu_state;

    const uint8_t opcode = 0b1110011;

    uint8_t rd = 0;
    uint8_t rs1 = 0;
    uint8_t funct3 = 0;
    uint16_t funct12 = 0;

    uint32_t instruction = 0;

    for (int i = 0; i < 1000; i++) {
        rd = uniform_rd_rs1(rand_engine);
        rs1 = uniform_rd_rs1(rand_engine);
        funct3 = uniform_funct3(rand_engine);
        funct12 = uniform_funct3(rand_engine);

        instruction = 0;

        instruction = instruction | (opcode & 0x7F);
        instruction = instruction | ((rd & 0x1F) << 7);
        instruction = instruction | ((funct3 & 0x07) << 12);
        instruction = instruction | ((rs1 & 0x1F) << 15);
        instruction = instruction | ((funct12 & 0xFFF) << 20);

        test_cpu_state = test_decoder.decode_instruction(instruction);

        EXPECT_EQ(test_cpu_state.instruction_opcode, opcode);
        EXPECT_EQ(test_cpu_state.dest_register_num, rd);
        EXPECT_EQ(test_cpu_state.dest_type, test_cpu_state.DEST_REGISTER);
        EXPECT_EQ(test_cpu_state.left_operand_register_num, rs1);
        EXPECT_EQ(test_cpu_state.left_operand_src_type, test_cpu_state.SRC_REGISTER);
        EXPECT_EQ(test_cpu_state.funct3, funct3);
        EXPECT_EQ(test_cpu_state.funct12, funct12);

        EXPECT_EQ(test_cpu_state.mem_op, test_cpu_state.MEM_OP_NONE);
        EXPECT_EQ(test_cpu_state.right_operand_register_num, 0);
        EXPECT_EQ(test_cpu_state.right_operand_src_type, test_cpu_state.SRC_NONE);
        EXPECT_EQ(test_cpu_state.funct7, 0);
        EXPECT_EQ(test_cpu_state.unsigned_immediate, 0);
        EXPECT_EQ(test_cpu_state.signed_immediate, 0);

        EXPECT_EQ(test_cpu_state.result, 0);
        EXPECT_EQ(test_cpu_state.memory_addr, 0);
        EXPECT_EQ(test_cpu_state.jump_addr, 0);
    }
}
