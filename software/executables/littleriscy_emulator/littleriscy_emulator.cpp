/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file src/littleriscy_emulator/littleriscy_emulator.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief RISC-V CPU Emulator supporting RV32I
 */

#include <cstdint>

#include <loguru.hpp>

#include "cpu_instruction_decode.h"
#include "cpu_instruction_execute.h"
#include "cpu_memory_operation.h"
#include "peripheral_init.h"
#include "ram_peripheral.h"
#include "cpu_register_file.h"

/**
 * @brief A RV32I instruction set emulator
 */
int main(int argc, char *argv[]) {
    loguru::init(argc, argv);
    loguru::g_flush_interval_ms = 100;
    loguru::add_file("littleriscy_emulator.log", loguru::Truncate, loguru::Verbosity_MAX);

    ram_peripheral instruction_memory(32768);
    ram_peripheral data_memory(32768);

    peripheral_init instruction_memory_loader("../test_firmware/bin/fibonacci.bin", &instruction_memory);

    cpu_register_file core_registers;

    // TODO(lao.alex.512): FIX - Temporarily set return address to end of memory so the loop quits
    core_registers.write_reg(1, 32768/4);
    // TODO(lao.alex.512): FIX - Temporarily set stack pointer to allocate a stack
    core_registers.write_reg(2, 1536);

    cpu_instruction_decode core_decoder;
    cpu_instruction_execute core_executor(&core_registers);
    cpu_memory_operation core_memory_operator(&data_memory);

    uint64_t instruction_count = 0;

    LOG_SCOPE_FUNCTION(INFO);

    while (instruction_memory.read(core_registers.program_counter/4) != 0) {
        ++instruction_count;
        LOG_F(
            1,
            "PROGRAM COUNTER: %d",
            core_registers.program_counter);

        cpu_state state = core_decoder.decode_instruction(instruction_memory.read(core_registers.program_counter/4));
        core_executor.execute_instruction(&state);
        core_memory_operator.execute_memory_operation(&state);

        switch (state.dest_type) {
            case cpu_state::DEST_REGISTER: {
                core_registers.write_reg(state.dest_register_num, state.result);
                break;
            }
        }

        if (state.instruction_opcode == cpu_state::OPCODE_JAL) {
            core_registers.program_counter = state.jump_addr;
        } else if (state.instruction_opcode == cpu_state::OPCODE_JALR) {
            core_registers.program_counter = state.jump_addr;
        } else if (state.instruction_opcode == cpu_state::OPCODE_BRANCH) {
            if (state.result) {
                core_registers.program_counter +=
                    state.signed_immediate;
            } else {
                core_registers.program_counter += 4;
            }
        } else {
            core_registers.program_counter += 4;
        }
    }

    int32_t result = data_memory.read(512/4);

    LOG_F(
        INFO,
        "RESULT AT ADDR 512: %d",
        result);

    LOG_F(
        INFO,
        "Total instructions executed: %ld",
        instruction_count);

    return 0;
}
