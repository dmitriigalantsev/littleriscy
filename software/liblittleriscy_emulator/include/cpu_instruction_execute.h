/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file include/cpu_instruction_execute.h
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief Header for cpu_instruction_execute.cpp
 */

#ifndef SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_INSTRUCTION_EXECUTE_H_
#define SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_INSTRUCTION_EXECUTE_H_

#include "cpu_register_file.h"
#include "cpu_state.h"
#include "ram_peripheral.h"

class cpu_instruction_execute {
 public:
    explicit cpu_instruction_execute(cpu_register_file* reg_file);
    void execute_instruction(cpu_state* state);
 private:
    cpu_register_file* reg_file;

    void execute_load(cpu_state* state);
    void execute_op_imm(cpu_state* state);
    void execute_auipc(cpu_state* state);
    void execute_store(cpu_state* state);
    void execute_op(cpu_state* state);
    void execute_lui(cpu_state* state);
    void execute_branch(cpu_state* state);
    void execute_jal(cpu_state* state);
    void execute_jalr(cpu_state* state);
    void execute_system(cpu_state* state);
};


#endif  // SOFTWARE_LIBLITTLERISCY_EMULATOR_INCLUDE_CPU_INSTRUCTION_EXECUTE_H_
