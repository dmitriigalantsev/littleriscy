/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file src/littleriscy_emulator/cpu_memory_operation.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief RISC-V memory operation stage
 */

#include <loguru.hpp>

#include "cpu_memory_operation.h"

/**
 * @brief Constructor
 */
cpu_memory_operation::cpu_memory_operation(
    ram_peripheral *data_memory_ptr
): data_memory_ptr(data_memory_ptr) {}

/**
 * @brief Execute a memory write back operation
 */
void cpu_memory_operation::execute_memory_operation(
    cpu_state *state  //!< Pointer to a cpu_state object that contains the state after instruction execute
) {
    LOG_SCOPE_FUNCTION(2);

    switch (state->mem_op) {
        case cpu_state::MEM_OP_WRITE: {
            LOG_F(
                3,
                "MEM OP STORE");

            this->data_memory_ptr->write(state->memory_addr/4, state->result);

            LOG_F(
                3,
                "WRITE %d TO MEM[%d]",
                state->result,
                state->memory_addr/4);
            break;
        }
        case cpu_state::MEM_OP_READ: {
            state->result = this->data_memory_ptr->read(state->memory_addr/4);

            // Load mask and sign extend
            switch (state->funct3) {
                case cpu_state::FUNCT3_LOAD_LB: {
                    LOG_F(
                        3,
                        "MEM OP LOAD BYTE - SIGN EXTEND");

                    state->result = (state->result & 0xFF) | ((state->result & 0x80) ? 0xFFFFFF00 : 0);
                    break;
                }
                case cpu_state::FUNCT3_LOAD_LH: {
                    LOG_F(
                        3,
                        "MEM OP LOAD HALF WORD - SIGN EXTEND");

                    state->result = (state->result & 0xFFFF) | ((state->result & 0x8000) ? 0xFFFF0000 : 0);
                    break;
                }
                case cpu_state::FUNCT3_LOAD_LW: {
                    LOG_F(
                        3,
                        "MEM OP LOAD WORD");
                    break;
                }
                case cpu_state::FUNCT3_LOAD_LBU: {
                    LOG_F(
                        3,
                        "MEM OP LOAD BYTE - UNSIGNED");

                    state->result = state->result & 0xFF;
                    break;
                }
                case cpu_state::FUNCT3_LOAD_LHU: {
                    LOG_F(
                        3,
                        "MEM OP LOAD HALF WORD - UNSIGNED");

                    state->result = state->result & 0xFFFF;
                    break;
                }
                default: {
                    LOG_F(
                        FATAL,
                        "Unimplementated memory operation");
                    break;
                }
            }

            LOG_F(
                3,
                "READ %d FROM MEM[%d]",
                state->result,
                state->memory_addr/4);
            break;
        }
    }
}
