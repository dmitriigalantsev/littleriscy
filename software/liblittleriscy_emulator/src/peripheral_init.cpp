/**
 *  MIT License
 *
 *  Copyright (c) 2020 Alex Lao
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 */

/**
 * @file src/littleriscy_emulator/peripheral_init.cpp
 *
 * @author Alex Lao <lao.alex.512@gmail.com>
 * @date   2020-2021
 *
 * @brief Initializes a peripheral by loading data from a file and writing to a peripheral
 */

#include <loguru.hpp>

#include "peripheral_init.h"

/**
 * @brief Constructor
 */
peripheral_init::peripheral_init(
    const char* input_filename,        //!< File to read data from
    peripheral* target_peripheral_ptr  //!< Pointer to target peripheral to write data to
) {
    input_file.open(input_filename, std::ios::binary | std::ios::ate);

    if (!input_file) {
        LOG_F(
            FATAL,
            "Unable to open %s for reading.",
            input_filename);
        return;
    }

    uint32_t input_file_size = input_file.tellg();

    if (target_peripheral_ptr->get_num_words() < input_file_size/4) {
        LOG_F(
            FATAL,
            "Target peripheral of size %d too small to intialize with file of size %d.",
            target_peripheral_ptr->get_num_words(),
            input_file_size/4);
        return;
    }

    input_file.seekg(0, std::ios::beg);

    uint32_t write_address = 0;
    uint32_t read_word;
    while (true) {
        input_file.read(reinterpret_cast<char *>(&read_word), 4);
        if (!input_file.good()) {
            break;
        }
        target_peripheral_ptr->write(write_address, read_word);
        write_address++;
    }

    input_file.close();
}
