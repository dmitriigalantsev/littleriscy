# Working Directory for Littleriscy Emulator
Run 'make' here to build RV32I binaries for testing purposes

## Dependencies
You must have 'riscv32-unknown-elf' build tools available in your PATH variable. See the top level README.md for more details.
