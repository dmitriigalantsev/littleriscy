# Littleriscy

Littleriscy is an SoC written in SystemVerilog (eventually) along with an instruction set emulator written in C++.

## License

This project is licensed under the MIT license. See LICENSE.txt for more details.

## Credits

* **Alex Lao** - Initial work - https://www.voltagedivice.com/
* **Dimitrii Galantsev** - Test binary makefiles - https://github.com/dmitrii-galantsev

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Building

To build all of Littleriscy.

```
git clone --recurse-submodules '<URL TO THIS REPO>'
cd littleriscy
mkdir build
cd build
cmake ../
make -j $(nproc)
```

### Running the Tests

Littleriscy uses GoogleTest as the testing infrastructure.

To run the tests for liblittleriscy_emulator from the build directory.

```
./test/software/liblittleriscy_emulator/liblittleriscy_emulator_gtest
```

### Building Test Firmware Binaries

Simple test firmware is in `test_firmware`.

The emulator has been tested with riscv32-unknown-elf tools built from https://github.com/riscv/riscv-gnu-toolchain.

To install a suitable toolchain version.

```
git clone --recursive https://github.com/riscv/riscv-gnu-toolchain
cd ./riscv-gnu-toolchain
git checkout 2022.11.18
mkdir build
cd ./build
../configure --prefix=/opt/riscv32 --with-arch=rv32i --with-abi=ilp32
make -j $(nproc)
```

Then add the tools to your path.

```
export PATH=/opt/riscv32/bin:$PATH
```

Build test binaries by running make in the `test_firmware` directory.

```
cd ./test_firmware
make
```

### Running the Emulator

Littleriscy contains an instruction set emulator.

To run the littleriscy_emulator from the build directory.

```
./software/executables/littleriscy_emulator/littleriscy_emulator
```

Test binary loaded by the emulator is hardcoded in the main().
